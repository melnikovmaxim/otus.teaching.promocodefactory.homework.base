﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data as IEnumerable<T>);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddEntityAsync(T entity)
        {
            var containsEntity = Data.Any(x => x.Id == entity.Id);

            if (containsEntity)
            {
                throw new ArgumentException($"Entity with guid '{entity.Id}' is already in the data");
            }
            
            Data.Add(entity);
            
            return Task.CompletedTask;
        }

        public Task UpdateEntityAsync(T entity)
        {
            var storedEntity = Data.First(x => x.Id == entity.Id);
            var isRemoved = Data.Remove(storedEntity);

            if (!isRemoved)
            {
                throw new ArgumentException($"Data does not contain entity with guid '{entity.Id}'");
            }
            
            Data.Add(entity);

            return Task.CompletedTask;
        }

        public Task RemoveEntityAsync(Guid entityId)
        {
            var storedEntity = Data.FirstOrDefault(x => x.Id == entityId);

            if (storedEntity is null)
            {
                throw new ArgumentException($"Data does not contain an entity with guid '{entityId}'");
            }
            
            Data.Remove(storedEntity);
            
            return Task.CompletedTask;
        }
    }
}